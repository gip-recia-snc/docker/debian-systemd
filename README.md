# debian-systemd

Docker Debian Stable Minimaliste avec Systemd fonctionnel


```
$ docker run --privileged \
    -d --rm -h debian.example.com \
    --name debian-testing -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
    gitlab.adullact.net:4567/gip-recia-snc/public/docker/debian-systemd

$ docker exec -it debian-testing /bin/bash


root@debian:/# systemctl status
● debian.example.com
    State: running
     Jobs: 0 queued
   Failed: 0 units
    Since: Mon 2019-04-15 14:44:38 UTC; 17s ago
   CGroup: /docker/4d51781658faf9785564b194548334b88ec89eee1fb6fb6b4a32e57d22a38d1c
           ├─22 /bin/bash
           ├─28 systemctl status
           ├─29 pager
           ├─init.scope
           │ └─1 /lib/systemd/systemd
           └─system.slice
             └─systemd-journald.service
               └─19 /lib/systemd/systemd-journald
```